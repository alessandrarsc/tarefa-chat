const msgs = document.querySelector('.msgs')
const btnEnviar = document.querySelector('#btnEnviar')
const mensagem = document.querySelector("#mensagem")

btnEnviar.addEventListener('click', function() { 
    
    const mensagemNova = document.createElement('div')
    msgs.appendChild(mensagemNova)
    mensagemNova.classList.add("espacoInterno")
    
    mensagemNova.innerHTML = `<div class="caixatextoAleatorio">
                                <textarea name="text" id="textoAleatorio">${mensagem.value}
                                </textarea> 
                              </div>`
    let editar = document.createElement('button')
    editar.classList.add('btnEditar')
    editar.innerText = 'Editar'
    mensagemNova.appendChild(editar)

    let excluir = document.createElement('button')
    excluir.classList.add('btnExcluir')
    excluir.innerText = 'Excluir'
    mensagemNova.appendChild(excluir)


    let btnExcluir = document.querySelector('.btnExcluir')

    excluir.addEventListener('click', function() { 
        mensagemNova.remove()
    })
})
